require 'pruby'

#
# Classe definissant diverses methodes pour calculer le nombre
# d'inversions dans un tableau d'elements Comparable.
#

class Array
  #
  # Methode sequentielle iterative.
  #
  def nb_inversions_seq
    nb_inversions_seq_ij( 0, size-1 )
  end

  def nb_inversions_seq_ij( i, j )
    (i...j).reduce(0) { |nb, k| self[k+1] < self[k] ? nb+1 : nb }
  end

  private :nb_inversions_seq_ij

  #
  #
  # Produit un Range qui indique les indices de la tranche attribuee
  # au k-ieme thread, en fonction du nombre de threads (nb_threads) et
  # de la taille du tableau (size, i.e., self.size).
  #
  # On suppose que la taille du tableau est divisible par le nombre de
  # threads (pre-condition).
  #
  def bornes_de_tranche3(k, n, nb_threads)
      b_inf = k * n / nb_threads
      b_sup = (k + 1) * n / nb_threads - 1

      b_inf..b_sup
  end

  def bornes_de_tranche( k, nb_threads )
    DBC.require( size % nb_threads == 0,
                 "*** #{nb_threads} ne divise pas #{size}" )

    (k * size / nb_threads..(k + 1) * size / nb_threads - 1)
  end

  private :bornes_de_tranche

  def mystere
      return 0 if empty?
      mystere_rec_ij(0, size - 1) {|x| yield(x)}
  end

  def mystere_rec_ij(i,j)
      #cas impair
      nb = 0
      if i == j
          nb += 1 if yield(self[i])
          return nb
      end

      #cas pair
      if j - i == 1
          nb += 1 if yield(self[i])
          nb += 1 if yield(self[j])
          return nb
      end

      m = (i + j) / 2

      nb_inf = PRuby.future {mystere_rec_ij(i,m){|x| yield(x)}}
      nb_sup = mystere_rec_ij(m + 1,j){|x| yield(x)}
      return nb_inf.value + nb_sup
  end
end

puts [10].mystere{|x| x}
puts [10, 20].mystere{|x| x}
puts [10, 20, 30, 40].mystere{|x| x < 2}
puts [10, 20, 30, 40, 50].mystere{|x| x}
puts [10, 20, 30, 40].mystere{|x| (x / 10).even? }
