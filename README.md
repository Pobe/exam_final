# Antoine LeBel / LEBA23068603

# Questions
- Hiver 2017:
    - 2 (Pruby: array)
    - 3 (TBB: polynomes)
    - 4 (OpenMP: Saxpy)
    - 6 (Diffusion chaleur)

- Hiver 2016:
    - 2a (OpenMP: index_of)
    - 4 (mesures de performance)
    - 5: (MPI: Saxpy)

# #2 (Pruby: array)
## i
R: Doit être égal à 4

R: Doit être égal à 2. Seulement les éléments divisé par 10 et pairs sont retenus.

R: Doit être égal à 20. Seulement les éléments plus petits ou égals à 20 sont retenus. Le tableau commence à 1, étendu jusqu'à 100.

## ii
Elle compte le nombre d'éléments dans le tableau selon une condition testée sur chacun d'entre eux. Elle pourrait s'appeller count(|item| block).

## iii
Non à cause de la valeur partagée nb. `peach` ou `peach_index` peuvent séparer le tableau et ainsi compter le nombre d'éléments pour chacune des parties mais remplace seulement la partie "map" du calcul. Il doit y avoir un tableau de sommation et un reduce sur celui-ci quand même pour bénéficier de parallélisme.

## iiii

*testé avec ruby*

```
def mystere_rec_ij(i,j)
  nb = 0

  #cas impair
  if i == j
      nb += 1 if yield(self[i])
      return nb
  end

  #cas pair
  if j - i == 1
      nb += 1 if yield(self[i])
      nb += 1 if yield(self[j])
      return nb
  end

  m = (i + j) / 2

  nb_inf = PRuby.future { mystere_rec_ij(i,m){|x| yield(x)} }
  nb_sup = mystere_rec_ij(m + 1,j){|x| yield(x)}
  return nb_inf.value + nb_sup
end
```
# #3 (TBB: Polynomes)

```
Polynome derive( Polynome p ) {
  if (p.degre == 1) {
    Polynome pPrime = allouer(1);
    pPrime.coefficients[0] = 0;
    return pPrime;
  }
  Polynome d = allouer(p.degre - 1);
  parallel_for(
      blocked_range<size_t>(1, p.degre),
      [&](blocked_range<size_t> r) {
        for( size_t i = r.begin(); i <= r.end(); i++ ) {
          d.coefficients[i - 1] = i * p.coefficients[i]; 
        }
      }
      );

  return d;
}
```

```
double valeur(Polynome p, double v) {
  return parallel_reduce(
      blocked_range<size_t>(0, p.degre),
      0.0,
      [=](blocked_range<size_t> r, double acc) {
        for(size_t i = r.begin(); i < r.end(); i++) {
          acc += p.coefficients[i] * std::pow(v, i);
        } 
        return acc;
      },
      std::plus<double>()
      );
}
```

# #4 (OpenMP: saxpy)

```
void saxpy(float a, float x[], float y[], int n) {

# pragma omp parallel for schedule( static )
    for(int i = 0; i < n; i++) {

        y[i] = a * x[i] + y[i];

    }
}
```

```
float produit_scalaire( float x[], float y[], int n) {
    float ps = 0.0;

# pragma omp for reduction(+:ps) schedule( static )
    for(int i = 0; i < n; i++) {

        ps += x[i] * y[i];
    }

    return ps;
}
```

# #6 (Diffusion chaleur)
Il y a deux approches qui paraissent adéquates, soit un parallélisme de tâches ou un parallélisme de résultats. Essentiellement, une approche par le parallélisme de tâches permet de prendre un ensemble de la grille de sorte qu'il soit relativement indépendant des autres. L'avantage ici est qu'il permet de limiter grandement la communication, le désavantage est la perte de précision concernant la dépendance des carrés aux frontières. La fonction Gauss-Seidel serait utilisé dans ce cas-ci. Quant au parallélisme de résultats, celui-ci permet d'offrir une meilleure diffusion de la valeur de chaque carré à un temps donnée sur l'ensemble de l'espace dynamiquement. Son désavantage est qu'il nécessite plus de communication entre les carrés et peut donc être moins performant. Le patron dans ce cas-ci serait un calcul "wavefront".

La première étape est de considérer comment le problème sera divisé, son partitionnement. Déjà montrée dans le schéma, on constate que la division sous forme de grille est un bon point de départ. Chaque "carré" de la grille possède des carrés voisins (nord, sud, est, ouest) et la moyenne de la chaleur des voisins à un temps donné permettra d'obtenir la chaleur du carré ciblé.

La tâche la plus fine est donc le calcul d'un seul carré. À ce stade, on peut parler de deux approches en ce qui à trait de la communication selon la granularité: soit un calcul de chaque carré indépendamment (granularité fine) ou soit un calcul d'un ensemble de carrés (granularité plus grossière). Dans ce cas-ci une granularité plus grossière me parait plus adéquate puisqu'elle évite le nombre de communication entre chacun des carrés. Cette manière de procéder se découpe d'ailleurs logiquement soit verticalement, horizontalement ou selon l'espace d'une source de chaleur. 

On sait que les positions verticales peuvent variées mais que les horizontales sont fixes. Il serait donc plus simple de distribuer les ensembles en fonction de colonnes puisque les calculs pour chacune d'entre elles prendraient essentiellement le même temps.

Puis, pour le mapping une association statique est plus adaptée pour les programmes de type SPMD.

En conclusion, un mélange des deux approches serait l'idéale, une zone frontière entre deux colonnes pourrait se calculer en mode parallélisme de tâches (Gauss-Seidel) tandis qu'à l'intérieur des frontières une approche "wavefront" permettrait d'offrir de meilleurs résultats.


# #2a (OpenMP: index_of)
## i
Pas une bonne stratégie, toutes les threads exécutent le code. Beaucoup de travail pour rien.

Elle produit un bon résultat.

## ii
Pas une bonne stratégie, `omp critical` demande l'accès à un verrou à chaque itération, c'est probablement plus lent qu'une boucle séquentiel. 

Elle produit un bon résultat.

### iii
Oui, elle retournera une valeur si au moins un élément respecte le prédicat.

Elle produit un bon résultat.

Il s'agit de la meilleure solution.

# #4 (mesures de performance)
## a
### i
150 / 40 = **3.75**

### ii
100 / 40 = **2.5**

### iii

Np.Proc    Acc. Abs                Eff

1          100 / 150 = 0.6666      0.6666

2          100 / 60  = 1.6666      0.83

4          100 / 40  = 2.5         0.625

8          100 / 50  = 2           0.25

2 processeurs offre la meilleure efficacité à **83%**.

### iv
L'accélération et l'efficacité tendront à diminuer. On constate qu'après 4 processeurs le temps d'éxécution semble augmenter et qu'à partir de 2 processeurs l'efficacité diminue. Ce comportement est classique des courbe en "U" des programmes en parallèle.

## b
95% du problème est parallélisable où le travail peut être divisé également entre les processeurs.

Pour avoir une accélération de 10 avec un problème "embarassingly parallel", il faudrait théoriquement 10 processeurs.

Du coup, pour combler le 5% séquentiel, il faudrait un peu plus de 10 processeurs, donc 11 au minimum pour avoir une accélération d'au moins 10.

# #5 (MPI: Saxpy)
```
void saxpy( float a, float x[], float y[], int n, int racine, MPI_Comm comm ) {
    MPI_Init(NULL, NULL);
    int world_rank;
    MPI_Comm_rank(comm, &world_rank);

    int world_size;
    MPI_Comm_size(comm, &world_size);

    assert(world_size % n == 0);
    int elem_proc = world_size / n;
    
    //Séparer le tableau en partie selon n
    float *subset_x = (float *)malloc(sizeof(float) * (elem_proc));
    float *subset_y = (float *)malloc(sizeof(float) * (elem_proc));

    //Envoyer les sous parties du calcul à chacun des processus
    MPI_BCast(a, 1, MPI_FLOAT, racine, comm); 
    MPI_Scatter(x, elem_proc, MPI_FLOAT, subset_x, elem_proc, MPI_FLOAT, racine, comm);
    MPI_Scatter(y, elem_proc, MPI_FLOAT, subset_y, elem_proc, MPI_FLOAT, racine, comm);

    //Calcul local
    for( int i = 0; i < elem_proc; i++ ) {
        (&subset_y)[i] = a*(&subset_x)[i] + (&subset_y)[i]; 
    }

    //Gather directement et seulement sur y pour sa nouvelle valeur
    MPI_Gather(&subset_y, 1, MPI_FLOAT, y, elem_proc, MPI_FLOAT, racine, comm);
}
```
